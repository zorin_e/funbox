$(function(){
	console.log('hello!');

	var text,
		description;

	// click eat
	$('.eat__container').on('click', function() {
		var parent = $(this).parent();

		if(!parent.hasClass('eat--disabled')) {
			if(!parent.hasClass('eat--selected')) {
				parent.addClass('eat--selected nothover');

				// add descirption
				description = parent.find('.eat__footer').html();
				parent.find('.eat__footer').empty().text(parent.find('.eat__footer').data('description'));

				// unlock hover
				$(this).mouseleave(function() {
					parent.removeClass('nothover')
				});
			}
			else {
				parent.removeClass('eat--selected');
				$(this).find('.eat__title').removeClass('eat__title--selected').text(text);

				// add default description
				parent.find('.eat__footer').html(description);
			}
		}
	});

	$('body').on('click', '.eat__buy', function() {
		$(this).parents('.eat').find('.eat__container').trigger('click');
	});

	// hover eat
	$('.eat__container').mouseenter(function() {
		var parent = $(this).parent(),
			textSelected = $(this).find('.eat__title').data('selected');

		if(!parent.hasClass('nothover') && parent.hasClass('eat--selected')) {
			text = $(this).find('.eat__title').text();
			$(this).find('.eat__title').addClass('eat__title--selected').text(textSelected);
		}
	}).mouseleave(function() {
		$(this).find('.eat__title').removeClass('eat__title--selected').text(text);
	});
});